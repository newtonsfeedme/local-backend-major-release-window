#!/bin/bash

trap '[ "$?" -eq 0 ] || read -p "Looks like something went wrong in step ´$STEP´... copy the screen and email support@newtons.tech. Press any key to continue..."' EXIT

#Quick Hack: used to convert e.g. "C:\Program Files\Docker Toolbox" to "/c/Program Files/Docker Toolbox"
win_to_unix_path(){ 
    wd="$(pwd)"
    cd "$1"
        the_path="$(pwd)"
    cd "$wd"
    echo $the_path
}

# This is needed  to ensure that binaries provided
# by Docker Toolbox over-ride binaries provided by
# Docker for Windows when launching using the Quickstart.
export PATH="$(win_to_unix_path "${DOCKER_TOOLBOX_INSTALL_PATH}"):$PATH"
VM=${DOCKER_MACHINE_NAME-default}
DOCKER_MACHINE="${DOCKER_TOOLBOX_INSTALL_PATH}\docker-machine.exe"
DOCKER_COMPOSE="${DOCKER_TOOLBOX_INSTALL_PATH}\docker-compose.exe"
DOCKER="${DOCKER_TOOLBOX_INSTALL_PATH}\docker.exe"
WD="$(win_to_unix_path "$(pwd)")"

STEP="Looking for vboxmanage.exe"
if [ ! -z "$VBOX_MSI_INSTALL_PATH" ]; then
  VBOXMANAGE="${VBOX_MSI_INSTALL_PATH}VBoxManage.exe"
else
  VBOXMANAGE="${VBOX_INSTALL_PATH}VBoxManage.exe"
fi

BLUE='\033[1;34m'
ORANGE='\033[1;33m'
GREEN='\033[0;32m'
NC='\033[0m'

#clear all_proxy if not socks address
if  [[ $ALL_PROXY != socks* ]]; then
  unset ALL_PROXY
fi
if  [[ $all_proxy != socks* ]]; then
  unset all_proxy
fi

if [ ! -f "${DOCKER_MACHINE}" ]; then
  echo "Docker Machine is not installed. Please re-run the Toolbox Installer and try again."
  exit 1
fi

if [ ! -f "${DOCKER_COMPOSE}" ]; then
  echo "Docker Compose is not installed. Please re-run the Toolbox Installer and try again."
  exit 1
fi

if [ ! -f "${DOCKER}" ]; then
  echo "Docker is not installed. Please re-run the Toolbox Installer and try again."
  exit 1
fi

if [ ! -f "${VBOXMANAGE}" ]; then
  echo "VirtualBox is not installed. Please re-run the Toolbox Installer and try again."
  exit 1
fi

"${VBOXMANAGE}" list vms | grep \""${VM}"\" &> /dev/null
VM_EXISTS_CODE=$?

set -e

STEP="Checking if machine $VM exists"
if [ $VM_EXISTS_CODE -eq 1 ]; then
  echo "VM not exist, please install and ensure everything work fine"
  exit 1
fi

STEP="Checking status on $VM"
VM_STATUS="$( set +e ; "${DOCKER_MACHINE}" status "${VM}" )"
if [ "${VM_STATUS}" != "Running" ]; then
  "${DOCKER_MACHINE}" start "${VM}"
  yes | "${DOCKER_MACHINE}" regenerate-certs "${VM}"
fi

STEP="Setting env"
eval "$("${DOCKER_MACHINE}" env --shell=bash --no-proxy "${VM}" | sed -e "s/export/SETX/g" | sed -e "s/=/ /g")" &> /dev/null #for persistent Environment Variables, available in next sessions
eval "$("${DOCKER_MACHINE}" env --shell=bash --no-proxy "${VM}")" #for transient Environment Variables, available in current session

STEP="Update Sequence: run update sequence inside container"
winpty "${DOCKER}" exec -it feedme_php-fpm_1 bash update.sh
winpty "${DOCKER}" exec -it feedme_php-fpm_1 bash update-script.sh

STEP="Finalize"
clear
cat << EOF
          ########
        ##        ##
      ##  ########  ##
     #    ########    #
    #     ##           #
   #      #######       #
   #      #######       #
   #      ##            #
   #      ## O D W      #
    #     ## # D #     #
     #    ## # D #    #
      ##  ## # D #  ##
        #### # # ###
          ########
EOF
echo -e "${ORANGE}Feedme Server${NC} has finished minor update"
echo
read -p "Press any key to exit..." EXIT 

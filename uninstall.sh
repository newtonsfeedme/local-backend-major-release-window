#!/bin/bash
trap '[ "$?" -eq 0 ] || read -p "Looks like something went wrong in step ´$STEP´... Press any key to continue..."' EXIT

#Quick Hack: used to convert e.g. "C:\Program Files\Docker Toolbox" to "/c/Program Files/Docker Toolbox"
win_to_unix_path(){ 
    wd="$(pwd)"
    cd "$1"
        the_path="$(pwd)"
    cd "$wd"
    echo $the_path
}

# This is needed  to ensure that binaries provided
# by Docker Toolbox over-ride binaries provided by
# Docker for Windows when launching using the Quickstart.
export PATH="$(win_to_unix_path "${DOCKER_TOOLBOX_INSTALL_PATH}"):$PATH"
VM=${DOCKER_MACHINE_NAME-default}
DOCKER_MACHINE="${DOCKER_TOOLBOX_INSTALL_PATH}\docker-machine.exe"

BLUE='\033[1;34m'
ORANGE='\033[1;33m'
GREEN='\033[0;32m'
NC='\033[0m'

while true; do
	echo "Do you seriously want to uninstall?"
	echo "All data and record will be deleted and it cannot be undone"
    read -p "proceed uninstall at your own risk? (y/n)" yn
    case $yn in
        [Yy]* )
			#clear all_proxy if not socks address
			if [ ! -f "${DOCKER_MACHINE}" ]; then
			  echo "Docker Machine is not installed. Please re-run the Toolbox Installer and try again."
			  exit 1
			fi

			set -e

			STEP="Delete and clean up $VM"
			"${DOCKER_MACHINE}" rm -f "${VM}" &> /dev/null || :
			rm -rf ~/.docker/machine/machines/"${VM}"
			
			STEP="Delete and clean up installed file"
			rm -rf ~/feedme-server

			STEP="Finalize"
			clear
			cat << EOF
          ########
        ##        ##
      ##  ########  ##
     #    ########    #
    #     ##           #
   #      #######       #
   #      #######       #
   #      ##            #
   #      ## O D W      #
    #     ## # D #     #
     #    ## # D #    #
      ##  ## # D #  ##
        #### # # ###
          ########
EOF
		echo -e "${ORANGE}Feedme Server${NC} successfully deleted!"
		echo "For help, please contact support@newtons.tech"
		echo
		echo 
		read -p "Press any key to exit..." EXIT
		break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done



